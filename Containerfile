ARG FEDORA_VERSION=40
ARG FEDORA_SHA=sha256:dc7b685f98a390c595f037f3cf5964ebe09b83c2586774fba3986210c03b93d2

FROM registry.fedoraproject.org/fedora:${FEDORA_VERSION}@${FEDORA_SHA} as builder

ARG CHARIOTT_VERSION="08e52b0cf01d3d4a1bade5a0c7c7181f5540d5b8"

RUN dnf -y install rust cargo unzip cmake protobuf-devel

ADD https://github.com/eclipse-chariott/chariott/archive/${CHARIOTT_VERSION}.zip /tmp/chariott.zip

RUN unzip /tmp/chariott.zip -d /tmp  && \
mv /tmp/chariott-${CHARIOTT_VERSION} /tmp/chariott && \
(cd /tmp/chariott; cargo build --release;)

FROM registry.fedoraproject.org/fedora:${FEDORA_VERSION}@${FEDORA_SHA}

# Copy our build
COPY --from=builder /tmp/chariott/target/release/chariott /usr/local/bin/chariott
COPY --from=builder /tmp/chariott/target/release/chariott.d /etc/

ENTRYPOINT ["/usr/local/bin/chariott"]
